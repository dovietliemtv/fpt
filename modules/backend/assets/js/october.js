/*
 * This is a bundle file, you can compile this in two ways:
 * (1) Using your favorite JS combiner
 * (2) Using CLI command:
 *   php artisan october:util compile assets
 *
 * @see october-min.js
 *

=require vendor/jquery.touchwipe.js
=require vendor/jquery.autoellipsis.js
=require vendor/jquery.waterfall.js
=require vendor/jquery.cookie.js
=require ../vendor/dropzone/dropzone.js
=require ../vendor/sweet-alert/sweet-alert.js
=require ../vendor/jcrop/js/jquery.Jcrop.js
=require ../../../system/assets/vendor/prettify/prettify.js
=require ../../widgets/mediamanager/assets/js/mediamanager-global.js

=require october.lang.js
=require october.alert.js
=require october.scrollpad.js
=require october.verticalmenu.js
=require october.navbar.js
=require october.sidenav.js
=require october.scrollbar.js
=require october.filelist.js
=require october.layout.js
=require october.sidepaneltab.js
=require october.simplelist.js
=require october.treelist.js
=require october.sidenav-tree.js
=require october.datetime.js

=require backend.js
*/

$(function() {
    $('.mainmenu-nav li').each(function(){
        var pages = ['student', 'lecturer', 'news', 'project', 'program'];
        for (var i = 0; i < pages.length; i++) {
            if ($(this).find("[data-menu-id='" + pages[i] + "']").length > 0) {
                $(this).css("display", "none");
            }
        }
    });
});
