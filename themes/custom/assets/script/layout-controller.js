(function(jQuery) {
	jQuery.fn.layoutController = function(options) {
		var defaults = {
			currentWidth: 0,
			currentHeight: 0,
			currentOrientation: null,
			newOrientation: null,
			onSmartphone: function(){},
			onTablet: function(){},
			onDesktop: function(){},
		}, opts = jQuery.extend(defaults, options);

		var win = $(window),
			layoutSmartphone = {
				minimumWidth: 0,
				maximumWidth: 767,
				orientation: 'smartphone'
			},
			layoutTablet = {
				minimumWidth: 768,
				maximumWidth: 1079,
				orientation: 'tablet'
			},
			layoutDesktop = {
				minimumWidth: 1080,
				maximumWidth: Number.POSITIVE_INFINITY,
				orientation: 'desktop'
			};

		function initialize() {
			opts.newOrientation = detectLayout();
			
			if(opts.currentOrientatio == null) {
				handleCurrentLayoutChange(opts.newOrientation);
				opts.currentOrientation = opts.newOrientation;
			} else {
				if(opts.newOrientation != opts.currentOrientation) {
					handleCurrentLayoutChange(opts.newOrientation);
					opts.currentOrientation = opts.newOrientation;
				}
			}
		}

		function detectLayout() {
			winWidth = win.width();
			winHeight = win.height();
			var getLayout = null;

			if(winWidth >= layoutSmartphone.minimumWidth && winWidth <= layoutSmartphone.maximumWidth) {
				getLayout = layoutSmartphone.orientation;
			} else {
				if(winWidth >= layoutTablet.minimumWidth && winWidth <= layoutTablet.maximumWidth) {
					getLayout = layoutTablet.orientation;
				} else {
					getLayout = layoutDesktop.orientation;
				}
			}

			return getLayout;
		}

		function handleCurrentLayoutChange(layout) {
			switch(layout) {
				case 'smartphone':
					opts.onSmartphone();
				break;
				case 'tablet':
					opts.onTablet();
				break;
				case 'desktop':
					opts.onDesktop();
				break;
				default:
				break;
			}
		}

		initialize();

		$(window).resize(function() {
			opts.newOrientation = detectLayout();

			if(opts.newOrientation != opts.currentOrientation) {
				handleCurrentLayoutChange(opts.newOrientation);
				opts.currentOrientation = opts.newOrientation;
			}
		});
	}
})(jQuery);