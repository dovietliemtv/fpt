let currentNewsSection = 1;
let currentProjectSection = 1;

let pageSize = 2;
const $body = $('body');


changeSlide = () => {
    $(`.fpt-s-project ul.animation-bg li:nth-child(${currentProjectSection})`).addClass('isActive');
    setTimeout(() => {
        $(`.fpt-s-project ul.animation-bg li:nth-child(${currentProjectSection})`).siblings().removeClass('isActive');
    }, 500)

    // news section
    $(`.fpt-s-exihibition ul.animation-bg li:nth-child(${currentNewsSection})`).addClass('isActive');
    setTimeout(() => {
        $(`.fpt-s-exihibition ul.animation-bg li:nth-child(${currentNewsSection})`).siblings().removeClass('isActive');
    }, 500)
}

handleToggleNav = () => {
    $('.js-open-nav').on('click', function (e) {
        e.preventDefault();
        $body.addClass('open-nav');
    })
    $('.js-close-nav').on('click', function (e) {
        e.preventDefault();
        $body.removeClass('open-nav');
    })
}


$(document).ready(function () {
    const $body = $('body');
    handleToggleNav();
    $(".js-left").on('click', function (e) {
        e.preventDefault();
        const dataSlider = $(this).data('slidername');
        const slideActive = $(this).siblings('.fpt-slider').find('.fpt-b-main-content.isActive');
        if (slideActive.prev().length !== 0) {
            slideActive.prev().addClass('isActive');
            slideActive.removeClass('isActive');
            $('.fpt-left-content').removeClass('animationIsActive');
            setTimeout(() => {
                $('.fpt-left-content').addClass('animationIsActive');
            }, 500)
            switch (dataSlider) {
                case 'project': {
                    currentProjectSection--;
                    break;
                }
                case 'news': {
                    currentNewsSection--;
                    break
                }
                default:
                    break
            }
            changeSlide();
        }
    })

    $(".js-right").on('click', function (e) {
        e.preventDefault();
        const dataSlider = $(this).data('slidername');
        const slideActive = $(this).siblings('.fpt-slider').find('.fpt-b-main-content.isActive');
        if (slideActive.next().length !== 0) {
            slideActive.next().addClass('isActive');
            slideActive.removeClass('isActive');
            $('.fpt-left-content').removeClass('animationIsActive');
            setTimeout(() => {
                $('.fpt-left-content').addClass('animationIsActive');
            }, 500)
            switch (dataSlider) {
                case 'project': {
                    currentProjectSection++;
                    break;
                }
                case 'news': {
                    currentNewsSection++;
                    break
                }
                default:
                    break
            }
            changeSlide();
        }

    })

    $(".fpt-pj-button").hover(
        function () {
            const dataId = $(this).data('id');
            $(`.fpt-paralax-bg-wrapper .fpt-paralax-bg:nth-child(${dataId})`).addClass('isActive');
            $(`.fpt-paralax-bg-wrapper .fpt-paralax-bg:nth-child(${dataId})`).siblings().removeClass('isActive');
        }
    );

    if ($('body').hasClass('homePage')) {
        $('#home-full-page').fullpage({
            navigation: true,
            lockAnchors: true,
            verticalCentered: false,
            scrollingSpeed: 1000,
            anchors: ['about us', 'featured project', 'news'],
            responsiveWidth: 1081,
            onLeave: function (origin, destination, direction) {

                $('.fpt-left-content').removeClass('animationIsActive');
            },
            afterLoad: function (origin, destination, direction) {

                const locale = $('#fpt-page-title').data('locale');
                if (locale === 'vi') {
                    switch (origin) {
                        case 'about us': {
                            $('#fpt-page-title').text('Giới thiệu')
                            break;
                        }
                        case 'featured project': {
                            $('#fpt-page-title').text('Dự án')

                            break;
                        }
                        case 'news': {
                            $('#fpt-page-title').text('Tin tức')

                            break;
                        }
                    }
                } else {
                    $('#fpt-page-title').text(origin)
                }



                $('.fpt-left-content').addClass('animationIsActive');
            },
            afterResponsive: function (isResponsive) {

            }
        });
    }

    $('.js-up').on('click', function (e) {
        e.preventDefault();
        $('#home-full-page').fullpage.moveSectionUp();
    });
    $('.js-down').on('click', function (e) {
        e.preventDefault();
        $('#home-full-page').fullpage.moveSectionDown();
    });



    if ($('body').hasClass('programDetailPage')) {
        $('#program-detail-full-page').fullpage({
            navigation: true,
            lockAnchors: false,
            verticalCentered: false,
            scrollingSpeed: 700,
            autoScrolling: false,
            fitToSection: false,
            responsiveWidth: 1081,
            anchors: ['hero', 'career-prospect', 'career-path', 'different-training', 'program-training', 'register', 'featured-projects'],
            afterLoad: function (origin, destination, direction) {

                $('.fpt-left-content').addClass('animationIsActive');
            },
        });
    }


    if ($('body').hasClass('newsDetailPage')
        || $('body').hasClass('careerDetailPage')
        || $('body').hasClass('studentDetailPage')
        || $('body').hasClass('lecturerDetailPage')
    ) {
        $('#detail-full-page').fullpage({
            navigation: true,
            lockAnchors: false,
            verticalCentered: false,
            scrollingSpeed: 700,
            autoScrolling: false,
            fitToSection: false,
            responsiveWidth: 1081,
            anchors: ['hero', 'content', 'featured-projects'],
            afterLoad: function (origin, destination, direction) {
                $('.fpt-left-content').addClass('animationIsActive');
            },
        });
    }
    if ($('body').hasClass('projectDetailPage')) {
        $('#project-detail-full-page').fullpage({
            navigation: true,
            lockAnchors: false,
            verticalCentered: false,
            scrollingSpeed: 700,
            autoScrolling: false,
            fitToSection: false,
            responsiveWidth: 1081,
            anchors: ['hero', 'content', 'featured-projects'],
            afterLoad: function (origin, destination, direction) {
                $('.fpt-left-content').addClass('animationIsActive');
            },
        });
    }


    $('.team-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1081,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    $('.feature-slider').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        draggable: false,
        responsive: [
            {
                breakpoint: 1081,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });


    $('.client-slider').slick({
        slidesToShow: 10,
        slidesToScroll: 5,
        autoplay: true,
        autoplaySpeed: 3000,
        rows: 2,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
              }
            }
          ]
      });
    // popup img
    $('.js-img-popup').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        zoom: {
            enabled: true,
            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out' // CSS transition
        }
    });


    $('.js-fpt-video').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'iframe',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        zoom: {
            enabled: true,
            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out' // CSS transition
        }
    });
    $body.waitForImages(function () {
        setTimeout(() => {
            $body.addClass('hide-loading');
            $body.removeClass('overflow-hidden');
        }, 1000);
        // animation
    });
    $('.customLink').on('click', function (e) {
        e.preventDefault();
        $body.addClass('hide-loading-lottie');
        if (e.currentTarget.href && e.currentTarget.href !== '#') {
            $body.addClass('overflow-hidden');

            setTimeout(() => {
                window.location.href = e.currentTarget.href;
            }, 500);
        }

    });


    $(window).scroll(function (event) {
        let scroll = $(window).scrollTop();
        if (scroll > 200) {
            $body.addClass('fpt-scroll-down');
        } else {
            $body.removeClass('fpt-scroll-down');
        }
    });

    window.addEventListener("pageshow", function (event) {
        var historyTraversal = event.persisted ||
            (typeof window.performance != "undefined" &&
                window.performance.navigation.type === 2);
        if (historyTraversal) {
            // Handle page restore.
            window.location.reload();
        }
    });

})


