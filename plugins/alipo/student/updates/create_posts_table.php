<?php namespace Alipo\Student\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_student_posts')){ 
            Schema::create('alipo_student_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
                $table->text('slug');
                $table->text('video_banner');
                $table->text('short_des');
                $table->text('color');
                $table->text('des');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_student_posts');
    }
}
