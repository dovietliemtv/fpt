<?php namespace Alipo\Career\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_career_posts')){ 
            Schema::create('alipo_career_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('video_banner');
                $table->text('short_des');
                $table->smallInteger('highlight');
                $table->text('color');
                $table->text('des');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_career_posts');
    }
}
