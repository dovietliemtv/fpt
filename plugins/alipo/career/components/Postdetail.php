<?php namespace Alipo\Career\Components;

use Cms\Classes\ComponentBase;
use Alipo\Career\Models\Post;

class Postdetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Postdetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['careerDetail'] = $this->loadPost();
        $slug = $this->param('slug');
        $post = Post::where('slug', '=', $slug)->first();
        $related = Post::whereHas('categories', function ($q) use ($post) {
            return $q->whereIn('slug', $post->categories->pluck('slug')); 
        })->where('id', '!=', $post->id)->get();

        $related3 = Post::whereHas('categories', function ($q) use ($post) {
            return $q->whereIn('slug', $post->categories->pluck('slug')); 
        })->where('id', '!=', $post->id)->paginate(3);

        $this->page['relatedCareer'] = $related;
        $this->page['relatedCareer3'] = $related3;
        

    }


    protected function loadPost()
    {
        $slug = $this->param('slug');

        $post = new Post;

        $post = $post->where('slug', $slug);

        $post = $post->first();

        return $post;
    }
}
