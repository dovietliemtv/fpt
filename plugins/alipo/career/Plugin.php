<?php namespace Alipo\Career;

use Backend;
use System\Classes\PluginBase;

/**
 * Career Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Career',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Career\Components\Post' => 'Career',
            'Alipo\Career\Components\Postdetail' => 'CareerDetail',
            'Alipo\Career\Components\Category' => 'Careercategory',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.career.some_permission' => [
                'tab' => 'Career',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'career' => [
                'label'       => 'Career',
                'url'         => Backend::url('alipo/career/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.career.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/career/post/'),
                        'permissions' => ['alipo.blog.post'],
                        'group'       => 'Post'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/career/category/'),
                        'permissions' => ['alipo.blog.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
