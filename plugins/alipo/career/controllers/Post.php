<?php namespace Alipo\Career\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Post Back-end Controller
 */
class Post extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Cms', 'cms', 'career-post');
    }
    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'thumb' && isset($record->thumb->path) ) {
            return '<img src="' . $record->thumb->path . '" width="50"/>';
        }
    }

}
