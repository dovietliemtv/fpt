<?php namespace Alipo\Project\Components;

use Cms\Classes\ComponentBase;
use Alipo\Project\Models\Post;
use Alipo\Project\Models\Category as CategoryModel;

class Category extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Category Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {

        parent::onRun(); // TODO: Change the autogenerated stub

        $slug = $this->param('slug');

        $related = Post::whereHas('categories', function ($q) use ($slug) {
            return $q->where('slug', $slug);
        })->get();

        $this->page['searchProject'] = $related;
        $this->page['categoriesProject'] = CategoryModel::all();

    }
}
