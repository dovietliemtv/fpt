<?php namespace Alipo\Lecturer;

use Backend;
use System\Classes\PluginBase;

/**
 * Lecturer Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Lecturer',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Alipo\Lecturer\Components\Postcp' => 'Lecturer',
            'Alipo\Lecturer\Components\Postcpdetail' => 'Lecturerdetail',
            'Alipo\Lecturer\Components\Categorycp' => 'Lecturercategory',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.lecturer.some_permission' => [
                'tab' => 'Lecturer',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'lecturer' => [
                'label'       => 'Lecturer',
                'url'         => Backend::url('alipo/lecturer/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.lecturer.*'],
                'order'       => 500,
            ],
        ];
    }
}
