<?php namespace Alipo\Lecturer\Components;

use Cms\Classes\ComponentBase;
use Alipo\Lecturer\Models\Post;

class Postcpdetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Postcpdetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['lecturerDetail'] = $this->loadPost();

        $post = $this->loadPost();
        $slug = $this->param('slug');
        $related3 = Post::whereHas('categories', function ($q) use ($post) {
            return $q->whereIn('slug', $post->categories->pluck('slug'));
        })->where('id', '!=', $post->id)->paginate(3);

        $this->page['lastestLecturer3'] = Post::where('slug', '!=', $slug)->get();

        $this->page['relatedLecturer3'] = $related3;
    }

    protected function loadPost()
    {
        $slug = $this->param('slug');

        $post = new Post;

        $post = $post->where('slug', $slug);

        $post = $post->first();

        return $post;
    }
}
