<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomepagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_homepages')){ 
            Schema::create('alipo_cms_homepages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('hero_title');
                $table->text('hero_des');
                $table->text('hero_url');
                $table->text('hero_content');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_homepages');
    }
}
