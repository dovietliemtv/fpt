<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneralsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_generals')){ 
            Schema::create('alipo_cms_generals', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('phone');
                $table->text('email');
                $table->text('address');
                $table->text('sns');
                $table->text('copyright');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_generals');
    }
}
