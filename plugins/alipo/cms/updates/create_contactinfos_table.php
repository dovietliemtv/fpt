<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactinfosTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_contactinfos')){
            Schema::create('alipo_cms_contactinfos', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->string('phone');
                $table->string('department');
                $table->string('city');
                $table->string('major');
                $table->string('campus');
                $table->string('birthday');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_contactinfos');
    }
}
