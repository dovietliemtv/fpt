<?php namespace Alipo\Cms;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Cms',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Cms', 'cms', 'plugins/alipo/cms/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Cms\Components\Homepage' => 'Homepage',
            'Alipo\Cms\Components\General' => 'General',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.cms.some_permission' => [
                'tab' => 'Cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Cms',
                'url'         => Backend::url('alipo/cms/general/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.cms.*'],
                'order'       => 500,
                'sideMenu' => [
                    'homepage' => [
                        'label'       => 'Home',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/homepage/update/1'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Page'
                    ],
                    'general' => [
                        'label'       => 'General',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/general/update/1'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'General'
                    ],
                    'contactinfo' => [
                        'label'       => 'Customer contact infomation',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/contactinfo'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'General'
                    ],
                    'blog-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/blog/post/'),
                        'permissions' => ['alipo.blog.*'],
                        'group'       => 'Blog'
                    ],
                    'blog-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/blog/category/'),
                        'permissions' => ['alipo.blog.*'],
                        'group'       => 'Blog'
                    ],
                    'career-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/career/post/'),
                        'permissions' => ['alipo.career.*'],
                        'group'       => 'Career'
                    ],
                    'career-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/career/category/'),
                        'permissions' => ['alipo.career.*'],
                        'group'       => 'Career'
                    ],
                    'program-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/program/post/'),
                        'permissions' => ['alipo.program.*'],
                        'group'       => 'Program'
                    ],
                    'program-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/program/category/'),
                        'permissions' => ['alipo.program.*'],
                        'group'       => 'Program'
                    ],
                    'project-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/post/'),
                        'permissions' => ['alipo.project.*'],
                        'group'       => 'Project'
                    ],
                    'project-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/category/'),
                        'permissions' => ['alipo.project.*'],
                        'group'       => 'Project'
                    ],
                    'lecturer-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/lecturer/post/'),
                        'permissions' => ['alipo.lecturer.*'],
                        'group'       => 'Lecturer'
                    ],
                    'lecturer-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/lecturer/category/'),
                        'permissions' => ['alipo.lecturer.*'],
                        'group'       => 'Lecturer'
                    ],
                    'student-post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/student/post/'),
                        'permissions' => ['alipo.student.*'],
                        'group'       => 'Student'
                    ],
                    'student-category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/student/category/'),
                        'permissions' => ['alipo.student.*'],
                        'group'       => 'Student'
                    ],
                ]
            ],
        ];
    }
}
