<?php namespace Alipo\Program\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_program_posts')){ 
            Schema::create('alipo_program_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->string('slug')->index();
                $table->string('video_banner');
                $table->string('lottie');
                $table->text('description');
                $table->string('color');
                $table->text('content');
                $table->text('content_1');
                $table->text('content_2');
                $table->text('careers');
                $table->text('training');
                $table->text('program');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_program_posts');
    }
}
