<?php namespace Alipo\Program;

use Backend;
use System\Classes\PluginBase;

/**
 * Program Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Program',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Program\Components\Post' => 'Program',
            'Alipo\Program\Components\Postdetail' => 'ProgramDetail',
            'Alipo\Program\Components\Category' => 'Programcategory',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.program.some_permission' => [
                'tab' => 'Program',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'program' => [
                'label'       => 'Program',
                'url'         => Backend::url('alipo/program/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.program.*'],
                'order'       => 500,
            ],
        ];
    }
}
