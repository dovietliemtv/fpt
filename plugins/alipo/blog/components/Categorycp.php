<?php namespace Alipo\Blog\Components;

use Cms\Classes\ComponentBase;
use Alipo\Blog\Models\Post;
use Alipo\Blog\Models\Category;


class Categorycp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Categorycp Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {

        parent::onRun(); // TODO: Change the autogenerated stub

        $slug = $this->param('slug');

        $related = Post::whereHas('categories', function ($q) use ($slug) {
            return $q->where('slug', $slug);
        })->get();

        $this->page['searchNews'] = $related;
        $this->page['categoriesNews'] = Category::all()->sortByDesc('created_at');

    }
}
