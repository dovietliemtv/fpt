<?php namespace Alipo\Blog;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Blog',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        // BackendMenu::registerContextSidenavPartial('Alipo.Blog', 'blog', 'plugins/alipo/blog/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Blog\Components\Postcp' => 'News',
            'Alipo\Blog\Components\Postcpdetail' => 'Newsdetail',
            'Alipo\Blog\Components\Categorycp' => 'Newscategory',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.blog.access_blog' => [
                'tab' => 'Blog',
                'label' => 'Edit Blog'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'blog' => [
                'label'       => 'Blog',
                'url'         => Backend::url('alipo/blog/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.blog.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/blog/post/'),
                        'permissions' => ['alipo.blog.post'],
                        'group'       => 'Post'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/blog/category/'),
                        'permissions' => ['alipo.blog.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
